const XMLNS = 'http://www.w3.org/2000/svg'

function createChild(tag, parent) {
  const child = document.createElementNS(XMLNS, tag)
  parent.appendChild(child);
  return child;
}

const root = document.documentElement;

const MONTH_NAME = 'July 2024';
const MONTH_STARTS_ON = 0;
const DAYS_IN_MONTH = 31;

const DAY_NAMES = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
const SCALE = 4;
const WIDTH = 297 * SCALE;
const HEIGHT = 210 * SCALE;
const MARGIN = [10 * SCALE, 10 * SCALE];
const HEADER_HEIGHT = 15 * SCALE;
const HEADER_TOP_PADDING = 0 * SCALE;
const HEADER_BOTTOM_PADDING = 2 * SCALE;
const BODY_WIDTH = WIDTH - MARGIN[0] * 2;
const BODY_HEIGHT = HEIGHT - MARGIN[1] * 2 - HEADER_HEIGHT;
const BOX_WIDTH = BODY_WIDTH / 7;
const BOX_HEIGHT = BODY_HEIGHT / 6;
const DAY_NUMBER_MARGIN = [1 * SCALE, 1 * SCALE];

root.setAttribute('width', WIDTH);
root.setAttribute('height', HEIGHT);

const rootPerimeter = createChild('rect', root);
rootPerimeter.id = 'root-perimeter';
rootPerimeter.classList.add('perimeter');
rootPerimeter.setAttribute('width', WIDTH);
rootPerimeter.setAttribute('height', HEIGHT);

const headerGroup = createChild('g', root);
headerGroup.setAttribute('transform', `translate(${MARGIN[0]}, ${MARGIN[1]})`);

const headerPerimeter = createChild('rect', headerGroup);
headerPerimeter.id = 'header-perimeter';
headerPerimeter.classList.add('perimeter');
headerPerimeter.setAttribute('width', BODY_WIDTH);
headerPerimeter.setAttribute('height', HEADER_HEIGHT);

const monthName = createChild('text', headerGroup);
monthName.classList.add('month-name');
monthName.textContent = MONTH_NAME;
monthName.setAttribute('alignment-baseline', 'hanging');
monthName.setAttribute('text-anchor', 'middle');
monthName.setAttribute('transform', `translate(${BODY_WIDTH / 2}, ${HEADER_TOP_PADDING})`);
monthName.setAttribute('font-size', 'larger');
monthName.setAttribute('alignment-baseline', 'hanging');

for (const [i, name] of DAY_NAMES.entries()) {
  const x = i * BOX_WIDTH + BOX_WIDTH / 2;
  const dayName = createChild('text', headerGroup);
  dayName.classList.add('day-name');
  dayName.textContent = name;
  dayName.setAttribute('text-anchor', 'middle');
  dayName.setAttribute('transform', `translate(${x}, ${HEADER_HEIGHT - HEADER_BOTTOM_PADDING})`);
  dayName.setAttribute('alignment-baseline', 'ideographic');
}

const bodyGroup = createChild('g', root);
bodyGroup.setAttribute('transform', `translate(${MARGIN[0]}, ${MARGIN[1] + HEADER_HEIGHT})`);

const bodyPerimeter = createChild('rect', bodyGroup);
bodyPerimeter.id = 'body-perimeter';
bodyPerimeter.classList.add('perimeter');
bodyPerimeter.setAttribute('width', BODY_WIDTH);
bodyPerimeter.setAttribute('height', BODY_HEIGHT);

for (let i = MONTH_STARTS_ON; i < DAYS_IN_MONTH + MONTH_STARTS_ON; i++) {
  const x = (i % 7) * BOX_WIDTH;
  const y = Math.floor(i / 7) * BOX_HEIGHT;

  const dayGroup = createChild('g', bodyGroup);
  dayGroup.setAttribute('transform', `translate(${x}, ${y})`);

  const dayBox = createChild('rect', dayGroup);
  dayBox.classList.add('day-box');
  dayBox.setAttribute('width', BOX_WIDTH);
  dayBox.setAttribute('height', BOX_HEIGHT);

  const dayNumber = createChild('text', dayGroup);
  dayNumber.classList.add('day-number');
  dayNumber.setAttribute('transform',
    `translate(${DAY_NUMBER_MARGIN[0]} ${DAY_NUMBER_MARGIN[1]})`
  );
  dayNumber.textContent = i + 1 - MONTH_STARTS_ON;
  dayNumber.setAttribute('alignment-baseline', 'hanging');
}
